﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    public abstract class Item
    {
        public String Name { get; set; }
        public int LevelRequirement { get; set; }
        public ItemSlot EquipedSlot { get; set; }
    }
}
