﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    public class Armour : Item
    {
        public ArmourType Type { get; set; }
        public PrimaryAttributes BonusAttributes { get; set; }
    }
}
