﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    public class Weapon : Item
    {
        public WeaponType Type { get; set; }
        public double Damage { get; set; }
        public double AttacksPerSecond { get; set; }

        /// <summary>
        /// Calculates the dps of the weapon by multiplying its damage with the attacks per second of the weapon and returns it as an int
        /// </summary>
        /// <returns>The weapons dps as an int</returns>
        public double GetDps()
        {
            return Damage * AttacksPerSecond;
        }

    }
}
