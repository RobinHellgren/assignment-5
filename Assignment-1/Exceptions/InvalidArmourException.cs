﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.Exceptions
{
    public class InvalidArmourException : Exception
    {
        public InvalidArmourException(String message): base(message){}
    }
}
