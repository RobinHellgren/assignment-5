﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.Exceptions
{
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException(String message) : base(message) {}
    }
}
