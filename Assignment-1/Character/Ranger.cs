﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    public class Ranger : Character
    {
        /// <summary>
        /// Sets default values for the characters base attributes, sets the total attributes to the base attributes, 
        /// sets the correct class label and sets ValidArmourTypes and ValidWeaponTypes to the ones specified for the ranger class
        /// </summary>
        public Ranger()
        {
            BaseAttributes = new PrimaryAttributes() { Vitality = 8, Strength = 1, Dexterity = 7, Intelligence = 1 };
            TotalAttributes = BaseAttributes;
            Class = "Ranger";
            ValidArmourTypes = new List<ArmourType>(){ ArmourType.Leather, ArmourType.Leather };
            ValidWeaponTypes = new List<WeaponType>(){ WeaponType.Bow };
        }
        /// <summary>
        /// Calculates the characters DPS by multiplying the damage of their weapon with it's attack speed, then multiplies the result with the damage factor for the ranger class (1 + Dexterity/100).
        /// If a weapon can't be found set the dps to 1 * 1 + Dexterity /100
        /// </summary>
        /// <returns>The characters DPS as an int</returns>
        public override double GetDps()
        {
            double dps;
            try
            {
                Weapon equipedWeapon = (Weapon)Equipment[ItemSlot.Weapon];
                dps = equipedWeapon.GetDps() * (1.0 + TotalAttributes.Dexterity / 100.0);
            }
            catch (Exception)
            {
                dps = 1 * (1 + TotalAttributes.Dexterity / 100.0);
            }
            return Math.Round(dps, 2);
        }
        /// <summary>
        /// Increases the characters base attributes with the attribute increase specified for the ranger class, increases the characters level value by one and updates thier total attributes by 
        /// calling the "UpdateTotalAttributes" method. 
        /// </summary>
        public override void LevelUp(int nrOfLevels)
        {
            if (nrOfLevels < 1)
            {
                throw new ArgumentException("Character can only level up one or more levels");
            }
            for (int i = 0; i < nrOfLevels; i++)
            {
            BaseAttributes += new PrimaryAttributes() { Vitality = 2, Strength = 1, Dexterity = 5, Intelligence = 1 };
            Level++;
            }
            UpdateTotalAttributes();
        }
    }
}
