﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    public class Mage : Character
    {
        /// <summary>
        /// Sets default values for the characters base attributes, sets the total attributes to the base attributes, 
        /// sets the correct class label and sets ValidArmourTypes and ValidWeaponTypes to the ones specified for the mage class
        /// </summary>
        public Mage()
        {
            BaseAttributes = new PrimaryAttributes() { Vitality = 5, Strength = 1, Dexterity = 1, Intelligence = 8 };
            TotalAttributes = BaseAttributes;
            Class = "Mage";
            ValidArmourTypes = new List<ArmourType>(){ArmourType.Cloth};
            ValidWeaponTypes = new List<WeaponType>(){WeaponType.Staff, WeaponType.Wand};
        }
        /// <summary>
        /// Calculates the characters DPS by multiplying the damage of their weapon with it's attack speed, then multiplies the result with the damage factor for the mage class (1 + Intelligence/100).
        /// If a weapon can't be found set the dps to 1 * 1 + Intelligence /100
        /// </summary>
        /// <returns>The characters DPS as an int</returns>
        public override double GetDps()
        {
            double dps;
            try
            {
                Weapon equipedWeapon = (Weapon)Equipment[ItemSlot.Weapon];
                dps = equipedWeapon.GetDps() * (1.0 + TotalAttributes.Intelligence / 100.0);
            }
            catch (Exception)
            {
                dps = 1 * (1 + TotalAttributes.Intelligence / 100.0);
            }
            return Math.Round(dps,2);
        }
        /// <summary>
        /// Increases the characters base attributes with the attribute increase specified for the mage class, increases the characters level value by one and updates thier total attributes by 
        /// calling the "UpdateTotalAttributes" method
        /// </summary>
        public override void LevelUp(int nrOfLevels)
        {
            if(nrOfLevels < 1)
            {
                throw new ArgumentException("Character can only level up one or more levels");
            }
            for (int i = 0; i < nrOfLevels; i++)
            {
            BaseAttributes += new PrimaryAttributes() { Vitality = 3, Strength = 1, Dexterity = 1, Intelligence = 5 };
            Level++;
            }
            UpdateTotalAttributes();
        }

    }
}
