﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{

    public abstract class Character
    {
        public Character()
        {
            Level = 1;
            Equipment = new Dictionary<ItemSlot, Item>();
        }
        public String Name { get; set; }
        public int Level { get; protected set; }
        public String Class { get; protected set; }
        public PrimaryAttributes BaseAttributes { get; protected set; }
        public PrimaryAttributes TotalAttributes { get; protected set; }
        public Dictionary<ItemSlot, Item> Equipment { get; private set; }
        public List<WeaponType> ValidWeaponTypes { get; protected set; }
        public List<ArmourType> ValidArmourTypes { get; protected set; }
        /// <summary>
        /// Calculates the characters health using the formula Vitality * 10 and returns it as an int.
        /// </summary>
        /// <returns>The total health of the character as an int</returns>
        public int GetHealth()
        {
            return TotalAttributes.Vitality * 10;
        }
        /// <summary>
        /// Calculates the characters armour using the formula strength + dexterity and returns it as an int.
        /// </summary>
        /// <returns>The total armour of the character as an int</returns>
        public int GetArmour()
        {
            return TotalAttributes.Strength + TotalAttributes.Dexterity;
        }
        /// <summary>
        /// Returns the character total elemental resistance, which is equal to their intelligence, as an int.
        /// </summary>
        /// <returns>The total elemental resistance of the character as an int</returns>
        public int GetElementalResistance()
        {
            return TotalAttributes.Intelligence;
        }
        /// <summary>
        /// Recalculates the characters current total attributes by adding the base attributes to all the bonus attributes from their equiped items
        /// </summary>
        protected void UpdateTotalAttributes()
        {
            TotalAttributes = BaseAttributes;

            foreach (KeyValuePair<ItemSlot, Item> keyValuePair in Equipment)
            {
                if (!(keyValuePair.Key == ItemSlot.Weapon))
                {
                    Armour equipedArmour = (Armour)keyValuePair.Value;
                    TotalAttributes += equipedArmour.BonusAttributes;
                }
            }
        }
        /// <summary>
        /// Equips a weapon on the character by adding it to the characters equipment dictonary, throws an "InvalidWeaponException" if the level requirement of the weapon exceeds 
        /// the characters current level or if the characters class can't use the weapon type of the weapon
        /// </summary>
        /// <param name="weapon">The weapon that is being equiped</param>
        public string Equip(Weapon weapon)
        {
            if (weapon.LevelRequirement > Level)
            {
                throw new Exceptions.InvalidWeaponException("The characters level is too low to equip this weapon");
            }
            if (!ValidWeaponTypes.Contains(weapon.Type))
            {
                throw new Exceptions.InvalidWeaponException("This characters class can't use this weapon");
            }
            if (Equipment.ContainsKey(ItemSlot.Weapon))
            {
                Equipment.Remove(ItemSlot.Weapon);
            }
            Equipment.Add(ItemSlot.Weapon, weapon);
            UpdateTotalAttributes();
            return "New weapon equiped!";
        }/// <summary>
         /// Equips a piece of armour to the character by adding it to the characters equipment dictonary, throws an "InvalidArmourException" if the level requirement of the armour exceeds
         /// he characters current level or if the characters class can't use the armour type of the armour
         /// </summary>
         /// <param name="armour">The armour that is being equiped</param>
        public string Equip(Armour armour)
        {
            if (armour.LevelRequirement > Level)
            {
                throw new Exceptions.InvalidArmourException("The characters level is too low to equip this armour");
            }
            if (!ValidArmourTypes.Contains(armour.Type))
            {
                throw new Exceptions.InvalidArmourException("This characters class can't use this weapon");
            }
            if (Equipment.ContainsKey(armour.EquipedSlot))
            {
                Equipment.Remove(armour.EquipedSlot);
            }
            Equipment.Add(armour.EquipedSlot, armour);
            UpdateTotalAttributes();
            return "New armour equiped!";
        }
        /// <summary>
        /// Increases the characters base attributes with the attribute increase specified for the specific class, increases the characters level value by one and updates thier total attributes by 
        /// calling the "UpdateTotalAttributes" method
        /// </summary>
        public abstract void LevelUp(int nrOfLevels);
        /// <summary>
        /// Calculates the characters DPS by multiplying the damage of their weapon with it's attack speed, then multiplies the result with the damage factor for specific class. Implemented in the 
        /// class specific subclass
        /// </summary>
        /// <returns></returns>
        public abstract double GetDps();
        /// <summary>
        /// Builds a string showing all the aspects of the character using a string builder and then returns the concatonated string.
        /// </summary>
        /// <returns>The characters character sheet as a string. </returns>
        public override string ToString()
        {
            StringBuilder sb = new();

            sb.AppendLine("********************************************");
            sb.AppendLine("\t" + "Name: " + Name);
            sb.AppendLine("\t" + "Class: " + Class);
            sb.AppendLine("\t" + "Level: " + Level);
            sb.AppendLine("");
            sb.AppendLine("\t" + "Base attributes:");
            sb.AppendLine("\t\t" + "Strength: " + BaseAttributes.Strength);
            sb.AppendLine("\t\t" + "Dexterity: " + BaseAttributes.Dexterity);
            sb.AppendLine("\t\t" + "Intelligence: " + BaseAttributes.Intelligence);
            sb.AppendLine("\t\t" + "Vitatlity: " + BaseAttributes.Vitality);
            sb.AppendLine("");
            sb.AppendLine("\t" + "Total attributes:");
            sb.AppendLine("\t\t" + "Strength: " + TotalAttributes.Strength);
            sb.AppendLine("\t\t" + "Dexterity: " + TotalAttributes.Dexterity);
            sb.AppendLine("\t\t" + "Intelligence: " + TotalAttributes.Intelligence);
            sb.AppendLine("\t\t" + "Vitatlity: " + TotalAttributes.Vitality);
            sb.AppendLine("");
            sb.AppendLine("\t" + "Secondary attributes");
            sb.AppendLine("\t\t" + "Health: " + GetHealth());
            sb.AppendLine("\t\t" + "Armour: " + GetArmour());
            sb.AppendLine("\t\t" + "Elemental Resistance: " + GetElementalResistance());
            sb.AppendLine("\t\t" + "DPS: " + GetDps());
            sb.AppendLine("");
            sb.AppendLine("\t" + "Equipment: ");
            foreach (KeyValuePair<ItemSlot, Item> keyValuePair in Equipment)
            {
                sb.AppendLine("\t\t" + keyValuePair.Key.ToString() + ": " + keyValuePair.Value.Name);
            }
            sb.AppendLine("********************************************");
            return sb.ToString();
        }
    }
}
