﻿using System;

namespace Assignment_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Armour cowl = new()
            {
                Type = ArmourType.Cloth,
                Name = "Rag",
                EquipedSlot = ItemSlot.Head,
                LevelRequirement = 1,
                BonusAttributes = new PrimaryAttributes()
                    { Strength = 0, Dexterity = 23, Intelligence = 54, Vitality = 22 }
            };
            Armour robe = new()
            {
                Type = ArmourType.Cloth,
                Name = "Shaggy robe",
                EquipedSlot = ItemSlot.Body,
                LevelRequirement = 1,
                BonusAttributes = new PrimaryAttributes()
                    { Strength = 0, Dexterity = 26, Intelligence = 52, Vitality = 25 }
            };
            Armour stainedPants = new()
            {
                Type = ArmourType.Cloth,
                Name = "Grandpa's pants",
                EquipedSlot = ItemSlot.Legs,
                LevelRequirement = 1,
                BonusAttributes = new PrimaryAttributes()
                    { Strength = 0, Dexterity = 22, Intelligence = 75, Vitality = 23 }
            };
            Weapon ricketyWand = new()
            {
                Type = WeaponType.Wand,
                Name = "Stick",
                EquipedSlot = ItemSlot.Weapon,
                LevelRequirement = 1, 
                AttacksPerSecond = 2, 
                Damage = 121
            };
            
            Mage mage = new Mage() { Name = "Göran" };

            Console.WriteLine(mage.Equip(cowl));
            Console.WriteLine(mage.Equip(robe));
            Console.WriteLine(mage.Equip(stainedPants));
            Console.WriteLine(mage.Equip(ricketyWand));


            Console.WriteLine(mage);
        }
    }
}
