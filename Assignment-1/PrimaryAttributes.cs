﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    public class PrimaryAttributes
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Vitality { get; set; }

        /// <summary>
        /// Sums two sets of primary attributes by adding each individual attribute in the first PrimaryAttributes class with the corresponding attribute in the second PrimaryAttributes class
        /// </summary>
        /// <param name="attributes1">First set of primary attribues</param>
        /// <param name="attributes2">Second set of primary attributes</param>
        /// <returns>A new set where each attribute from the params has been summed</returns>
        public static PrimaryAttributes operator +(PrimaryAttributes attributes1, PrimaryAttributes attributes2)
        {
            PrimaryAttributes summedAttributes = new()
            {
                Strength = attributes1.Strength + attributes2.Strength,
                Dexterity = attributes1.Dexterity + attributes2.Dexterity,
                Intelligence = attributes1.Intelligence + attributes2.Intelligence,
                Vitality = attributes1.Vitality + attributes2.Vitality
            };

            return summedAttributes;

        }
    }
}
