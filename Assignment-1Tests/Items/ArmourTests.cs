﻿using System;
using Xunit;
using FluentAssertions;

namespace Assignment_1Tests
{
    public class ArmourTests
    {
        [Fact]
        public void Equip_WarriorEquipsTooHighLevelArmour_ShouldThrowInvalidArmourException()
        {
            //Arrange
            Assignment_1.Warrior character = new();
            Assignment_1.Armour testPlateBody = new()
            {
                Name = "Common plate",
                LevelRequirement = 2,
                EquipedSlot = Assignment_1.ItemSlot.Body,
                Type = Assignment_1.ArmourType.Plate,
                BonusAttributes = new Assignment_1.PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };

            //Act & Assert
            Assert.Throws<Assignment_1.Exceptions.InvalidArmourException>(() => character.Equip(testPlateBody));
        }

        [Fact]
        public void Equip_WarriorEquipsCloth_ShouldThrowInvalidAmrourException()
        {
            //Arrange
            Assignment_1.Warrior character = new();
            Assignment_1.Armour testClothBody = new()
            {
                Name = "Common robe",
                LevelRequirement = 2,
                EquipedSlot = Assignment_1.ItemSlot.Body,
                Type = Assignment_1.ArmourType.Plate,
                BonusAttributes = new Assignment_1.PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };

            //Act & Assert
            Assert.Throws<Assignment_1.Exceptions.InvalidArmourException>(() => character.Equip(testClothBody));
        }

        [Fact]
        public void Equip_WarriorEquipsCorrectArmour_ShouldReturnSuccsessMessage()
        {
            //Arrange
            Assignment_1.Warrior character = new();
            Assignment_1.Armour testPlateBody = new()
            {
                Name = "Common plate",
                LevelRequirement = 1,
                EquipedSlot = Assignment_1.ItemSlot.Body,
                Type = Assignment_1.ArmourType.Plate,
                BonusAttributes = new Assignment_1.PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            String expectedMessage = "New armour equiped!";

            //Act
            String actualMessage = character.Equip(testPlateBody);

            //Assert
            Assert.Equal(expectedMessage, actualMessage);
        }

        [Fact]
        public void Equip_ArmourIsEquiped_ShouldCountAttributesFromArmour()
        {
            //Arrange
            Assignment_1.Warrior character = new();
            Assignment_1.PrimaryAttributes expectedAttributes = new() { Vitality = 12, Strength = 6, Dexterity = 3, Intelligence = 2 };
            Assignment_1.Armour testPlateBody = new()
            {
                Name = "Common plate",
                LevelRequirement = 1,
                EquipedSlot = Assignment_1.ItemSlot.Body,
                Type = Assignment_1.ArmourType.Plate,
                BonusAttributes = new Assignment_1.PrimaryAttributes() { Vitality = 2, Strength = 1, Dexterity = 1, Intelligence = 1 }
            };
            character.Equip(testPlateBody);
            //Act
            Assignment_1.PrimaryAttributes actualAttributes = character.TotalAttributes;

            //Assert
            expectedAttributes.Should().BeEquivalentTo(actualAttributes);
        }

        [Fact]
        public void GetDps_WeaponAndArmourIsEquiped_ShouldCountDpsFromWeaponAndAttributesFromArmour()
        {
            //Arrange
            Assignment_1.Warrior character = new();
            double expectedDps = Math.Round(7.7 * (1.0 + (6.0 / 100.0)), 2);
            Assignment_1.Weapon testAxe = new()
            {
                Name = "Common axe",
                LevelRequirement = 1,
                EquipedSlot = Assignment_1.ItemSlot.Weapon,
                AttacksPerSecond = 1.1,
                Damage = 7,
                Type = Assignment_1.WeaponType.Axe
            };
            Assignment_1.Armour testPlateBody = new()
            {
                Name = "Common plate",
                LevelRequirement = 1,
                EquipedSlot = Assignment_1.ItemSlot.Body,
                Type = Assignment_1.ArmourType.Plate,
                BonusAttributes = new Assignment_1.PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            character.Equip(testAxe);
            character.Equip(testPlateBody);
            //Act
            double actualDps = character.GetDps();

            //Assert
            Assert.Equal(expectedDps, actualDps);
        }
    }
}

