﻿using System;
using Xunit;

namespace Assignment_1Tests
{
    public class WeaponTests
    {
        [Fact]
        public void Equip_WarriorEquipsTooHighLevelWeapon_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Assignment_1.Warrior character = new();
            Assignment_1.Weapon testAxe = new()
            {
                Name = "Common axe",
                LevelRequirement = 2,
                EquipedSlot = Assignment_1.ItemSlot.Weapon,
                AttacksPerSecond = 1.1,
                Damage = 7,
                Type = Assignment_1.WeaponType.Axe
            };

            //Act & Assert
            Assert.Throws<Assignment_1.Exceptions.InvalidWeaponException>(() => character.Equip(testAxe));
        }

        [Fact]
        public void Equip_WarriorEquipsStaff_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Assignment_1.Warrior character = new();
            Assignment_1.Weapon testBow = new()
            {
                Name = "Common bow",
                LevelRequirement = 1,
                EquipedSlot = Assignment_1.ItemSlot.Weapon,
                AttacksPerSecond = 1.1,
                Damage = 7,
                Type = Assignment_1.WeaponType.Bow
            };

            //Act & Assert
            Assert.Throws<Assignment_1.Exceptions.InvalidWeaponException>(() => character.Equip(testBow));
        }

        [Fact]
        public void Equip_WarriorEquipsCorrectWeapon_ShouldReturnSuccessMessage()
        {
            //Arrange
            Assignment_1.Warrior character = new();
            Assignment_1.Weapon testAxe = new()
            {
                Name = "Common axe",
                LevelRequirement = 1,
                EquipedSlot = Assignment_1.ItemSlot.Weapon,
                AttacksPerSecond = 1.1,
                Damage = 7,
                Type = Assignment_1.WeaponType.Axe
            };
            String expectedMessage = "New weapon equiped!";

            //Act
            String actualMessage = character.Equip(testAxe);

            //Assert
            Assert.Equal(expectedMessage, actualMessage);
        }

        [Fact]
        public void GetDps_NoWeaponIsEquiped_ShouldCountDpsFromWeaponAsOne()
        {
            //Arrange
            Assignment_1.Warrior character = new();
            double expectedDps = 1.0*(1.0+(5.0/100.0));

            //Act
            double actualDps = character.GetDps();

            //Assert
            Assert.Equal(expectedDps, actualDps);
        }

        [Fact]
        public void GetDps_WeaponIsEquiped_ShouldCountDpsFromWeapon()
        {
            //Arrange
            Assignment_1.Warrior character = new();
            double expectedDps = Math.Round(7.7 * (1.0 + (5.0 / 100.0)), 2);
            Assignment_1.Weapon testAxe = new()
            {
                Name = "Common axe",
                LevelRequirement = 1,
                EquipedSlot = Assignment_1.ItemSlot.Weapon,
                AttacksPerSecond = 1.1,
                Damage = 7,
                Type = Assignment_1.WeaponType.Axe
            };
            character.Equip(testAxe);

            //Act
            double actualDps = character.GetDps();

            //Assert
            Assert.Equal(expectedDps, actualDps);
        }
        
    }
}

