﻿using System;
using Xunit;

namespace Assignment_1Tests
{
    public class CharacterTests
    {
        [Fact]
        public void CharacterConstructor_CharacterCreated_ShouldBeLevelOne()
        {
            //Arrange
            Assignment_1.Mage character = new Assignment_1.Mage();
            int expectedLevel = 1;

            //Act
            int actualLevel = character.Level;

            //Assert
            Assert.Equal(expectedLevel, actualLevel);
        }
        [Fact]
        public void GetHealth_CorrectHealthAfterLevelUp_ShouldHaveCorrectUpdatedHealth()
        {
            //Arrange
            Assignment_1.Mage character = new();
            character.LevelUp(1);

            int expectedHealth = 80;

            //Act
            int actualHealth = character.GetHealth();

            //Assert
            Assert.Equal(expectedHealth, actualHealth);
        }
        [Fact]
        public void GetArmour_CorrectArmourAfterLevelUp_ShouldHaveCorrectAmour()
        {
            //Arrange
            Assignment_1.Mage character = new();
            character.LevelUp(1);

            int expectedArmour = 4;

            //Act
            int actualArmour = character.GetArmour();

            //Assert
            Assert.Equal(expectedArmour, actualArmour);
        }
        [Fact]
        public void GetElementalResistance_CorrectElementalResistenceAfterLevelUp_ShouldHaveCorrectElementalResistance()
        {
            //Arrange
            Assignment_1.Mage character = new();
            character.LevelUp(1);

            int expectedElementalResistance = 13;

            //Act
            int actualElementalResistance = character.GetElementalResistance();

            //Assert
            Assert.Equal(expectedElementalResistance, actualElementalResistance);
        }
    }
}
