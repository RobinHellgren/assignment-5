﻿using System;
using Xunit;
using FluentAssertions;
namespace Assignment_1Tests
{
    public class MageTests
    {
        [Fact]
        public void LevelUp_MageIsLeveledUp_ShouldBeLevelTwo()
        {
            //Arrange
            Assignment_1.Mage character = new Assignment_1.Mage();
            character.LevelUp(1);
            int expectedLevel = 2;

            //Act
            int actualLevel = character.Level;

            //Assert
            Assert.Equal(expectedLevel, actualLevel);
        }
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void LevelUp_GainsLessThanOneLevels_ShouldThrowArgumentException(int nrOfLevels)
        {
            //Arrange
            Assignment_1.Mage character = new Assignment_1.Mage();

            //Act & Assert
            Assert.Throws<ArgumentException>(() => character.LevelUp(nrOfLevels));
        }
        [Fact]
        public void LevelUp_IsLeveledUp_ShouldHaveDefaultBaseAttributes()
        {
            //Arrange
            Assignment_1.Mage character = new();
            character.LevelUp(1);
            Assignment_1.PrimaryAttributes expectedDefaultBaseAttributes = new() { Vitality = 8, Strength = 2, Dexterity = 2, Intelligence = 13 };

            //Act
            Assignment_1.PrimaryAttributes actualBaseAttributes = character.BaseAttributes;

            //Assert
            expectedDefaultBaseAttributes.Should().BeEquivalentTo(actualBaseAttributes);
        }
        [Fact]
        public void Constructor_IsCreated_ShouldHaveDefaultBaseAttributes()
        {
            //Arrange
            Assignment_1.Mage character = new();
            Assignment_1.PrimaryAttributes expectedDefaultBaseAttributes = new() { Vitality = 5, Strength = 1, Dexterity = 1, Intelligence = 8 };

            //Act
            Assignment_1.PrimaryAttributes actualBaseAttributes = character.BaseAttributes;

            //Assert
            expectedDefaultBaseAttributes.Should().BeEquivalentTo(actualBaseAttributes);
        }

    }
}
