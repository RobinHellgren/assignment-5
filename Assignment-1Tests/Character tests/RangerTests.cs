﻿using System;
using Xunit;
using FluentAssertions;
namespace Assignment_1Tests
{
    public class RangerTests
    {
        [Fact]
        public void LevelUp_IsLeveledUp_ShouldBeLevelTwo()
        {
            //Arrange
            Assignment_1.Ranger character = new();
            character.LevelUp(1);
            int expectedLevel = 2;

            //Act
            int actualLevel = character.Level;

            //Assert
            Assert.Equal(expectedLevel, actualLevel);
        }
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void LevelUp_GainsLessThanOneLevels_ShouldThrowArgumentException(int nrOfLevels)
        {
            //Arrange
            Assignment_1.Ranger character = new ();

            //Act & Assert
            Assert.Throws<ArgumentException>(() => character.LevelUp(nrOfLevels));
        }
        [Fact]
        public void LevelUp_IsLeveledUp_ShouldHaveDefaultBaseAttributes()
        {
            //Arrange
            Assignment_1.Ranger character = new();
            character.LevelUp(1);
            Assignment_1.PrimaryAttributes expectedDefaultBaseAttributes = new() { Vitality = 10, Strength = 2, Dexterity = 12, Intelligence = 2 };

            //Act
            Assignment_1.PrimaryAttributes actualBaseAttributes = character.BaseAttributes;

            //Assert
            expectedDefaultBaseAttributes.Should().BeEquivalentTo(actualBaseAttributes);
        }
        [Fact]
        public void Constructor_IsCreated_ShouldHaveDefaultBaseAttributes()
        {
            //Arrange
            Assignment_1.Ranger character = new();
            Assignment_1.PrimaryAttributes expectedDefaultBaseAttributes = new() { Vitality = 8, Strength = 1, Dexterity = 7, Intelligence = 1 };

            //Act
            Assignment_1.PrimaryAttributes actualBaseAttributes = character.BaseAttributes;

            //Assert
            expectedDefaultBaseAttributes.Should().BeEquivalentTo(actualBaseAttributes);
        }
        [Fact]
        public void GetHealth_CorrectHealthAfterLevelUp_ShouldHaveCorrectUpdatedHealth()
        {
            //Arrange
            Assignment_1.Ranger character = new();
            character.LevelUp(1);

            int expectedHealth = 100;

            //Act
            int actualHealth = character.GetHealth();

            //Assert
            Assert.Equal(expectedHealth, actualHealth);
        }
    }
}
