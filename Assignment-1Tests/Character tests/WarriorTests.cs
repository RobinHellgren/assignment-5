﻿using System;
using Xunit;
using FluentAssertions;
namespace Assignment_1Tests
{
    public class WarriorTests
    {
        [Fact]
        public void LevelUp_WarriorIsLeveledUp_ShouldBeLevelTwo()
        {
            //Arrange
            Assignment_1.Mage character = new Assignment_1.Mage();
            character.LevelUp(1);
            int expectedLevel = 2;

            //Act
            int actualLevel = character.Level;

            //Assert
            Assert.Equal(expectedLevel, actualLevel);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void LevelUp_GainsLessThanOneLevels_ShouldThrowArgumentException(int nrOfLevels)
        {
            //Arrange
            Assignment_1.Warrior character = new Assignment_1.Warrior();

            //Act & Assert
            Assert.Throws<ArgumentException>(() => character.LevelUp(nrOfLevels));
        }

        [Fact]
        public void LevelUp_IsLeveledUp_ShouldHaveCorrectBaseAttributes()
        {
            //Arrange
            Assignment_1.Warrior character = new();
            character.LevelUp(1);
            Assignment_1.PrimaryAttributes expectedDefaultBaseAttributes = new() { Vitality = 15, Strength = 8, Dexterity = 4, Intelligence = 2 };

            //Act
            Assignment_1.PrimaryAttributes actualBaseAttributes = character.BaseAttributes;

            //Assert
            expectedDefaultBaseAttributes.Should().BeEquivalentTo(actualBaseAttributes);
        }

        [Fact]
        public void Constructor_IsCreated_ShouldHaveDefaultBaseAttributes()
        {
            //Arrange
            Assignment_1.Warrior character = new();
            Assignment_1.PrimaryAttributes expectedDefaultBaseAttributes = new() { Vitality = 10, Strength = 5, Dexterity = 2, Intelligence = 1 };

            //Act
            Assignment_1.PrimaryAttributes actualBaseAttributes = character.BaseAttributes;

            //Assert
            expectedDefaultBaseAttributes.Should().BeEquivalentTo(actualBaseAttributes);
        }
        [Fact]
        public void GetHealth_CorrectHealthAfterLevelUp_ShouldHaveCorrectUpdatedHealth()
        {
            //Arrange
            Assignment_1.Warrior character = new();
            character.LevelUp(1);

            int expectedHealth = 150;

            //Act
            int actualHealth = character.GetHealth();

            //Assert
            Assert.Equal(expectedHealth, actualHealth);
        }
    }
}
